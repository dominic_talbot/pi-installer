__author__ = 'dominic'

#import serial, sys, string
import httplib
import time
import re



# To do, look for file change on .live and only do something if it changes,
# handle any path via a settings.py file


# Domain you want to post to: localhost would be an emoncms installation on your own laptop
# this could be changed to emoncms.org to post to emoncms.org
domain = "172.16.16.17"

# Location of emoncms in your server, the standard setup is to place it in a folder called emoncms
# To post to emoncms.org change this to blank: ""
emoncmspath = "emoncms"

# Write apikey of emoncms account
apikey = "92026283167f33fa4338b9426dc97f85"

# Node id youd like the emontx to appear as
nodeid = 10


# Value type
value_type = 'power'


#conn = httplib.HTTPConnection(domain)

owl_live_filename = '.live'

owl_match = re.compile(r"(\d+/\d+/\d+\d+ \d+:\d+)\s-\s(\d+.\d+)")

# Sleep for the cm160 process to stablize
time.sleep(120)

while 1:
    # Open file and read contents
    fo = open(owl_live_filename, "r")
    string = fo.read()
    fo.close()
    print string

    initial_datetime = ''
    extracted_value = ''
    epoch_value = ''

    object_match = owl_match.search(string)
    if object_match:
        print object_match.group(1)
        print object_match.group(2)
        initial_datetime = object_match.group(1)
        extracted_value = object_match.group(2)

    if initial_datetime:
        print "we have an initial datetime"
        epoch_value = time.mktime(time.strptime(initial_datetime, "%d/%m/%Y %H:%M"))
        epoch_value = str(epoch_value)[:-2]
        print epoch_value
    conn = httplib.HTTPConnection(domain)
    conn.request("GET", "/"+emoncmspath+"/input/post.json?apikey="+apikey+"&node="+str(nodeid)+"&json={"+value_type+":"+extracted_value+"}")
    response = conn.getresponse()
    print response.read()
    time.sleep(60)
