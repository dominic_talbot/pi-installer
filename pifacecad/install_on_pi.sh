sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install python-pifacecad -y

# For python 3 use:
#sudo apt-get install python{,3}-pifacecad

#testing can be done the following way:
#python3 /usr/share/doc/python3-pifacecad/examples/sysinfo.py
