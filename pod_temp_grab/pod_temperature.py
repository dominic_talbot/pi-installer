import tmp100
import smbus
#import serial, sys, string
import httplib
import time
import re

address = 0b1001000 #address 0x48

# Domain you want to post to: localhost would be an emoncms installation on your own laptop
# this could be changed to emoncms.org to post to emoncms.org
domain = "172.16.16.17"

# Location of emoncms in your server, the standard setup is to place it in a folder called emoncms
# To post to emoncms.org change this to blank: ""
emoncmspath = "emoncms"

# Write apikey of emoncms account
apikey = "237adb642213dde6ebd1125f69ac7593"

# Node id youd like the emontx to appear as
nodeid = 10


# Value type
value_type = 'temperature'


while 1:
    initial_value = tmp100.Temp100()
    extracted_value = str(initial_value.getTemperature())

    conn = httplib.HTTPConnection(domain)
    conn.request("GET", "/"+emoncmspath+"/input/post.json?apikey="+apikey+"&node="+str(nodeid)+"&json={"+value_type+":"+extracted_value+"}")
    response = conn.getresponse()
    print response.read()
    time.sleep(30)

