#!/bin/bash
#You first need to run sudo rpi-update to enable the new 90fps camera modes
# 90fps mode only works well in good light. Dont use it in darkness.
#
echo "4" > /sys/class/gpio/export
echo "in" > /sys/class/gpio/gpio4/direction

while true;  do
 	trap 'echo "4" > /sys/class/gpio/unexport' 0
	stat=`cat /sys/class/gpio/gpio4/value`


while [ $stat = "1" ]
	do
	d=`date +%d%m%y`
	t=`date +%T`
	raspivid -vf -hf -w 640 -h 480 -fps 90 -o 90fps$d$t.h264 -t 10000
	stat='0'
	done
done
exit 0
