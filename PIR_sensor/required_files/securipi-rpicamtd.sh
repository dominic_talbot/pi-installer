echo "4" > /sys/class/gpio/export
echo "in" > /sys/class/gpio/gpio4/direction

while true;  do
 	trap 'echo "4" > /sys/class/gpio/unexport' 0
	stat=`cat /sys/class/gpio/gpio4/value`
while [ $stat = "1" ]
	do
	d=`date +%d%m%y`
	t=`date +%T`
	raspistill -o 1.jpg -w 1024 -h 768 -q 30
	raspivid -o $d$t.h264 -t 10000
	convert -pointsize 20 -fill yellow -draw 'text 850,30 "'$t' '$d'"' 1.jpg $d$t.jpg
	mpack -s "alarm photo" /home/pi/$d$t.jpg you@youremailaddress.co.uk
	stat='0'
	done
done
exit 0
