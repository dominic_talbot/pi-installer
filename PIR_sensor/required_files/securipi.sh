echo "4" > /sys/class/gpio/export
echo "in" > /sys/class/gpio/gpio4/direction

while true;  do
 	trap 'echo "4" > /sys/class/gpio/unexport' 0
	stat=`cat /sys/class/gpio/gpio4/value`
	while [ $stat = "1" ]
	do
		d=`date +%d%m%y`
		t=`date +%T`
		echo "Motion Detected $t $d" | mail -s "Motion Detected" youremailaddress@gmail.com
		echo "Motion Detected $t $d"
		echo "Motion Detected $t" >> log$d.txt
		stat='0'
	done
done
exit 0
