Important information about Gmail account for Raspberry Pi.

You need to setup a brand new Gmail account just for your Pi to use to send emails.

In addition to the information in the instructions, when you've setup the new Gmail account using your PC, with the account logged in, open another browser window & go to:

https://www.google.com/settings/u/0/security/lesssecureapps

and check the Enable radio button.

Also, when choosing a password for the Gmail account don't use the # symbol.
Password test9247!# would fail to send, where test9247!! would work just fine. (try it)


