echo "4" > /sys/class/gpio/export
echo "in" > /sys/class/gpio/gpio4/direction

while true;  do
 	trap 'echo "4" > /sys/class/gpio/unexport' 0
	stat=`cat /sys/class/gpio/gpio4/value`
	while [ $stat = "1" ]
	do
			d=`date +%d%m%y`
			t=`date +%T`
			raspistill -o $t$d.jpg -w 1024 -h 768 -q 30 -hf
			echo "Movement Detected $t $d" | mail -s "Movement Detected" youremailaddress@gmail.com
			echo "Movement Detected $t $d"
			echo "Movement Detected $t" >> log$d.txt
			raspivid -o $t$d.h264 -t 10000
			mpack -s "Movement Detected photo" $t$d.jpg youremailaddress@gmail.com
			stat="0"
			sleep 20
	done
done
exit 0
