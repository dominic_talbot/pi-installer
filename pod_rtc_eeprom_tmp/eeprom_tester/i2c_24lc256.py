#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" B025 - RTC/EEPROM/TMP Example For the 24LC256 EEPROM
    Copyright (c) 2013 Ciseco Ltd.
    
    Author: Matt Lloyd
    
    Requires wiringpi2-python for i2c interface
    
    This code is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    
"""
import wiringpi2

class EEPROM:
    def __init__(self):
        self.deviceAddress = 0x50
        self.i2c = wiringpi2.I2C()
        self.dev = self.i2c.setup(self.deviceAddress)

    def eepromRead(self, memoryAddress, byteCount):
        self.i2c.write(self.dev, (memoryAddress & 0x00FF00) >> 8)
        self.i2c.write(self.dev, (memoryAddress & 0x0000FF) >> 0)
        byteArray = list()
        for n in range(byteCount):
            byteArray.insert(n, self.i2c.read(self.dev))
        return byteArray
    
    def eepromReadByte(self, memoryAddress):
        return int(self.eepromRead(memoryAddress, 1)[0] & 0x0000FF)
    
    def eepromReadInt(self, memoryAddress):
        byteArray = self.eepromRead(memoryAddress, 2)
        return int(byteArray[0]<<8) | int(byteArray[1] <<0)
    
    def eepromWrite(self, memoryAddress, byteArray):
        self.i2c.write(self.dev, (memoryAddress & 0x00FF00) >> 8 )
        self.i2c.write(self.dev, (memoryAddress & 0x0000FF) >> 0 )
        #print(byteArray)
        for n in range(len(byteArray)):
            self.i2c.write(self.dev, byteArray[n])
    
    def eepromWriteByte(self, memoryAddress, byteToWrite):
        self.eepromWrite(memoryAddress, bytearray([byteToWrite]))
        
    def eepromWriteInt(self, memoryAddress, intToWrite):
        self.eepromWrite(memoryAddress,
                         bytearray([(intToWrite >> 8),(intToWrite & 0x0000FF)])
                         )

    def eepromTest(self):
        print("Writing Byte's to EEPROM")
        for memoryAddressCount in range(0, 255):
            self.eepromWriteByte(memoryAddressCount, memoryAddressCount)
        print("Done Byte Write")
        
        print("Reading Byte's form EEPROM")
        for memoryAddressCount in range(0, 255):
            print("Byte: {}, {}.".format(memoryAddressCount,
                                         self.eepromReadByte(memoryAddressCount)
                                         )
                  )
        print("Done Byte Read")

        print("Write Int's to EEPROM")
        for memoryAddressCount in range(0, 1023):
            self.eepromWriteInt(memoryAddressCount*2, memoryAddressCount)
        print("Done Int Write")

        print("Reading Int's from EEPROM")
        for memoryAddressCount in range(0, 1023):
            print("Int: {}, {}.".format(memoryAddressCount,
                                        self.eepromReadInt(memoryAddressCount * 2)
                                        )
                  )

if __name__ == "__main__":
    test = EEPROM()
    test.eepromTest()