import logging
import logging.handlers

my_logger = logging.getLogger('MyLogger')
my_logger.setLevel(logging.DEBUG)


#handler = logging.handlers.SysLogHandler(address = '/dev/log')

hostname = "specialhostname"
formatter = logging.Formatter('{}:%(asctime)s %(name)s: %(levelname)s %(message)s'.format(hostname), '%b %e %H:%M:%S')

handler = logging.handlers.SysLogHandler(address=('172.16.16.13', 514), facility=19)

handler.setFormatter(formatter)

my_logger.addHandler(handler)

my_logger.debug('Rocker man!!!!')
#my_logger.critical('this is critical')

