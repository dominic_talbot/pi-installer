#!/usr/bin/env python

# If ping works, then we do a
# command execution, if that works then we continue...
#


import logging
from subprocess import Popen, PIPE, STDOUT
import platform
import paramiko
import StringIO
from pexpect import pxssh

checked_platform = platform.system()


class Error(Exception):
    pass


class ConnectError(Error):
    pass


class AuthError(Error):
    pass




def connect_ssh(
    ip, username, password=None, client=paramiko.SSHClient,
    key=None, timeout=15.0,
):
    ssh = client()
    ssh.set_log_channel('critical_only')
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    if key:
        f = StringIO.StringIO(key)
        pkey = paramiko.DSSKey(file_obj=f)
    else:
        pkey = None
    try:
        ssh.connect(
            ip, username=username, password=password, pkey=pkey,
            timeout=timeout,
        )
    except (paramiko.AuthenticationException, EOFError) as e:
        raise AuthError(str(e))
    return ssh


def ssh_host_discover(ssh):
    stdin, stdout, stderr = ssh.exec_command("uname -a")
    stdin.write('\n')
    stdin.flush()

    return stdin, stdout, stderr



def short_ping(ipaddress):

    cmd = "ping -c 3 "+ipaddress

    output = Popen(cmd, stdout=PIPE, stderr=STDOUT, shell=True).communicate()[0]

    if checked_platform == 'Linux':
        if 'Unreachable' in output:
            logging.debug('Host seems offline')
            return "offline"
        else:
            logging.debug('Host is online')
            return "online"

    if checked_platform == 'Darwin':
        if 'Request timeout' in output:
            logging.debug('Host seems offline')
            return "offline"
        else:
            logging.debug('Host is online')
            return "online"

if __name__ == "__main__":
    print "Hello pooh head"

    ip = '172.16.17.19'
    result = short_ping(ip)
    print result
    print checked_platform


    try:
        username = 'pi'
        password = 'raspberry'
        ip = '172.16.17.19'
        ssh = connect_ssh(ip, username, password)
        print "Connection good, we can go onto checking the type of SSH host"

        stdin, stdout, stderr = ssh_host_discover(ssh)
        result = list(stdout.readlines())
        print result

    except:
        print "O poo"