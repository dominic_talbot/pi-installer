#!/bin/bash

# Make slow logging enabled 
sudo  sed -i -e 's|T0:23:respawn:/sbin/getty -L ttyAMA0 115200 vt100|#T0:23:respawn:/sbin/getty -L ttyAMA0 115200 vt100|' /etc/inittab 

sudo shutdown -r now
