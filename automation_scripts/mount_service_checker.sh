#!/bin/bash

# This script has been written to check if certain mount points and processors are running/mounted..

function_PROCESS_CHECK_AND_START () {
CMD=$2
PROCESS=$1

echo "Checking the following process: $PROCESS"

check_result=`pidof "$PROCESS"`

echo $check_result

if [[ -z "$check_result" ]]
then
echo "It does not seem to be running, lets start It"
`$CMD`


else echo "Its running, no need to do anything!!!"
fi

}

function_CHECK_DIR_EMPTY () {
CMD=$2
DIR=$1
# init
# look for empty dir
if [ "$(ls -A $DIR)" ]; then
     echo "No need, $DIR not Empty"
else
    echo "$DIR is Empty"
    echo "Going to run the command: $2"
    `$CMD`

fi
# rest of the logic

}


function_CHECK_DIR_EMPTY "/mnt/4TB" "mount -t ext4 /dev/sdb1 /mnt/4TB"
function_CHECK_DIR_EMPTY "/mnt/NAS_USBdisk1" "mount -t cifs //172.16.16.13/usbshare2-2 /mnt/NAS_USBdisk1 -o username=admin,password=nut4Jugen,uid=dominic,gid=dominic"
function_CHECK_DIR_EMPTY "/mnt/Store" "mount -t cifs //172.16.16.75/Store /mnt/Store/ -o username=admin,password=admin,uid=dominic,gid=dominic"
function_PROCESS_CHECK_AND_START "/usr/lib/plexmediaserver/Plex Media Server" "service plexmediaserver start"