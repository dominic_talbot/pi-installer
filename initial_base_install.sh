#!/bin/bash


#
#
# Adjust the user pi, which is our default to not have to use sudo password 
#sudo visudo, add the following line at the end:
#pi     ALL=(ALL) NOPASSWD:ALL


# base upgrade
sudo apt-get update
sudo apt-get upgrade -y

# Dev type tools
sudo apt-get install vim -y
sudo apt-get install git -y

# Serial interface tool
sudo apt-get install minicom -y

# Mosh, different terminal interface
sudo apt-get install mosh -y

# Misc tools
sudo apt-get install tree -y
sudo apt-get install byobu -y

#Monitoring tool
sudo apt-get install monitorix -y

# In some cases you might need to change monitorix's default port number of: 8080, for Traefix
#
# go to: /etc/monitorix
# sudo vi monitorix.conf, change default port, set to 8081
# sudo service monitorix restart


# Install Python 3
sudo apt-get install libffi-dev libssl-dev -y
sudo apt install python3-dev -y
sudo apt-get install -y python3 python3-pip


# Install curl if not included
sudo apt-get install curl -y

# Install the NTP replacement: chrony
sudo apt install chrony -y
sudo systemctl enable chrony
sudo systemctl start chrony

chronyc activity
chronyc sources -v
sleep 10



# Install Docker
curl -sSL https://get.docker.com | sh
sudo usermod -aG docker ${USER}
groups ${USER}

sudo pip3 install docker-compose

newgrp docker


# create structure to kick off things for reverse proxy
mkdir ~/docker
sudo setfacl -Rdm g:docker:rwx ~/docker
sudo chmod -R 775 ~/docker


# Prep for the Traefik reverse proxy, move to new file when ready:
sudo apt-get install apache2-utils -y


mkdir ~/docker/traefik2
mkdir ~/docker/shared

cd ~/docker/shared
echo $(htpasswd -nb pi nut4Jugen) | sed -e s/\\$/\\$\\$/g > .htpasswd


sudo apt-get install hd-idle -y

# change the default options to it logs to a log file

# make sure you set ....

#log in and out
#docker run hello-world


############## Old....
#sudo apt-get install putty -y
#sudo apt-get install arduino arduino-core  -y
#sudo apt-get install python-pip -y
#sudo init 6
